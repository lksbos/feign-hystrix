#Feign + Hystrix

Example project using Spring boot 2.0 + Open Feign and Hystrix.

The project also contains unit tests with jacoco configured.

##Build and download dependencies:
- ./gradlew clean build

##Run:
* ./gradlew bootRun