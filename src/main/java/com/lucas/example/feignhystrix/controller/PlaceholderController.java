package com.lucas.example.feignhystrix.controller;

import com.lucas.example.feignhystrix.externalservice.response.PostResponse;
import com.lucas.example.feignhystrix.externalservice.response.UserResponse;
import com.lucas.example.feignhystrix.externalservice.service.JsonPlaceholderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lucas on 02/05/18.
 */
@RestController
public class PlaceholderController {

    private JsonPlaceholderService service;

    @Autowired
    public PlaceholderController (JsonPlaceholderService service) {
        this.service = service;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/posts", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PostResponse> getPosts() {
        return service.getPosts();
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/posts/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PostResponse getPost(@PathVariable("id") String id) {
        return service.getPost(id);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserResponse> getUsers() {
        return service.getUsers();
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse getUser(@PathVariable("id") String id) {
        return service.getUser(id);
    }
}
