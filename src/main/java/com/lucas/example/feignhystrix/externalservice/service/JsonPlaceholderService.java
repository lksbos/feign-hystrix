package com.lucas.example.feignhystrix.externalservice.service;

import com.lucas.example.feignhystrix.externalservice.client.JsonPlaceholderClient;
import com.lucas.example.feignhystrix.externalservice.response.PostResponse;
import com.lucas.example.feignhystrix.externalservice.response.UserResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JsonPlaceholderService {
    private JsonPlaceholderClient client;

    public JsonPlaceholderService(JsonPlaceholderClient client) {
        this.client = client;
    }

    public List<PostResponse> getPosts() {
        return client.getPosts();
    }

    public PostResponse getPost(String id) {
        return client.getPost(id);
    }

    public List<UserResponse> getUsers() {
        return client.getUsers();
    }

    public UserResponse getUser(String id) {
        return client.getUser(id);
    }
}
