package com.lucas.example.feignhystrix.externalservice.client;

import com.lucas.example.feignhystrix.externalservice.response.PostResponse;
import com.lucas.example.feignhystrix.externalservice.response.UserResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Created by lucas on 02/05/18.
 */
@Component
@Slf4j
public class JsonPlaceholderClientFallback implements JsonPlaceholderClient {
    @Override
    public List<PostResponse> getPosts() {
        return Collections.emptyList();
    }
    @Override
    public PostResponse getPost(String id) {
        return null;
    }
    @Override
    public List<UserResponse> getUsers() {
        return Collections.emptyList();
    }
    @Override
    public UserResponse getUser(String id) {
        log.error(String.format("Is there a problem to access /users/%s", id));
        return null;
    }
}
