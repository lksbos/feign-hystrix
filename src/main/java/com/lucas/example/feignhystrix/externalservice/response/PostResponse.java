package com.lucas.example.feignhystrix.externalservice.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by lucas on 02/05/18.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PostResponse {
    private String id;
    private String userId;
    private String title;
    private String body;
}
