package com.lucas.example.feignhystrix.externalservice.client;

import com.lucas.example.feignhystrix.externalservice.response.PostResponse;
import com.lucas.example.feignhystrix.externalservice.response.UserResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by lucas on 02/05/18.
 */

@FeignClient(
        name = "json-placeholder",
        url = "https://jsonplaceholder.typicode.com",
        fallback = JsonPlaceholderClientFallback.class
)
public interface JsonPlaceholderClient {

    String USER_PATH = "/users/{id}";
    String USERS_PATH = "/users";
    String POST_PATH = "/posts/{id}";
    String POSTS_PATH = "/posts";

    @RequestMapping(method = RequestMethod.GET, value = POSTS_PATH)
    List<PostResponse> getPosts();
    @RequestMapping(method = RequestMethod.GET, value = POST_PATH)
    PostResponse getPost(@PathVariable("id") String id);
    @RequestMapping(method = RequestMethod.GET, value = USERS_PATH)
    List<UserResponse> getUsers();
    @RequestMapping(method = RequestMethod.GET, value = USER_PATH)
    UserResponse getUser(@PathVariable("id") String id);
}
