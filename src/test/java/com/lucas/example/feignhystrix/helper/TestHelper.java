package com.lucas.example.feignhystrix.helper;

import com.lucas.example.feignhystrix.externalservice.response.PostResponse;
import com.lucas.example.feignhystrix.externalservice.response.UserResponse;

import java.util.ArrayList;
import java.util.List;

public final class TestHelper {
    private TestHelper() {
        //intentionally empty
    }
    public static List<UserResponse> createMockUserList(int qty) {
        List<UserResponse> userResponses = new ArrayList<>();
        for(int i = 1; i <= qty; i++) {
            UserResponse userResponse = UserResponse.builder()
                    .id(String.valueOf(i))
                    .name("name " + i)
                    .username("username " + i)
                    .email("email " + i)
                    .build();
            userResponses.add(userResponse);
        }
        return userResponses;
    }

    public static List<PostResponse> createMockPostList(int qty) {
        List<PostResponse> postResponses = new ArrayList<>();
        for(int i = 1; i <= qty; i++) {
            PostResponse postResponse = PostResponse.builder()
                    .id(String.valueOf(i))
                    .userId("userId " + i)
                    .title("title " + i)
                    .body("body " + i)
                    .build();
            postResponses.add(postResponse);
        }
        return postResponses;
    }
}
