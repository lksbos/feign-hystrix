package com.lucas.example.feignhystrix;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class FeignHystrixApplicationTests {

	@Test
	public void contextLoads() {
		FeignHystrixApplication.main(new String[]{});
	}

}
