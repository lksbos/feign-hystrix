package com.lucas.example.feignhystrix.client;

import com.lucas.example.feignhystrix.externalservice.client.JsonPlaceholderClientFallback;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class JsonPlaceholderClientFallbackTest {
    private JsonPlaceholderClientFallback fallback;

    @Before
    public void setUp() {
        fallback = new JsonPlaceholderClientFallback();
    }

    @Test
    public void testGetPosts() {
        assertThat(fallback.getPosts(), is(emptyIterable()));
    }

    @Test
    public void testGetUsers() {
        assertThat(fallback.getUsers(), is(emptyIterable()));
    }

    @Test
    public void testGetPost() {
        assertThat(fallback.getPost(null), is(nullValue()));
    }

    @Test
    public void testGetUser() {
        assertThat(fallback.getUser(null), is(nullValue()));
    }
}
