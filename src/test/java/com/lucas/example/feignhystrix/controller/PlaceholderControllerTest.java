package com.lucas.example.feignhystrix.controller;

import com.lucas.example.feignhystrix.externalservice.client.JsonPlaceholderClient;
import com.lucas.example.feignhystrix.externalservice.response.PostResponse;
import com.lucas.example.feignhystrix.externalservice.response.UserResponse;
import com.lucas.example.feignhystrix.externalservice.service.JsonPlaceholderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.lucas.example.feignhystrix.helper.TestHelper.createMockPostList;
import static com.lucas.example.feignhystrix.helper.TestHelper.createMockUserList;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class PlaceholderControllerTest {
    private MockMvc mockMvc;

    @Mock
    private JsonPlaceholderService service;

    @Before
    public void setup() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(new PlaceholderController(service))
                        .build();
    }

    @Test
    public void testGetUser() throws Exception {
        UserResponse userResponse = createMockUserList(1).get(0);

        when(service.getUser("1")).thenReturn(userResponse);

        mockMvc
                .perform(get(JsonPlaceholderClient.USER_PATH.replace("{id}", "1")))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("id", is("1")));
    }

    @Test
    public void testGetUsers() throws Exception {
        List<UserResponse> userResponses = createMockUserList(2);

        when(service.getUsers()).thenReturn(userResponses);

        mockMvc
                .perform(get(JsonPlaceholderClient.USERS_PATH))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("[0].id", is("1")))
                .andExpect(jsonPath("[1].id", is("2")));
    }

    @Test
    public void testGetPost() throws Exception {
        PostResponse postResponse = createMockPostList(1).get(0);

        when(service.getPost("1")).thenReturn(postResponse);

        mockMvc
                .perform(get(JsonPlaceholderClient.POST_PATH.replace("{id}", "1")))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("id", is("1")));
    }

    @Test
    public void testGetPosts() throws Exception {
        List<PostResponse> userResponses = createMockPostList(2);

        when(service.getPosts()).thenReturn(userResponses);

        mockMvc
                .perform(get(JsonPlaceholderClient.POSTS_PATH))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("[0].id", is("1")))
                .andExpect(jsonPath("[1].id", is("2")));
    }

}
