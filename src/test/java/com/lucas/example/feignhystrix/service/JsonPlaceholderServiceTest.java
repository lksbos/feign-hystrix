package com.lucas.example.feignhystrix.service;

import com.lucas.example.feignhystrix.externalservice.client.JsonPlaceholderClient;
import com.lucas.example.feignhystrix.externalservice.response.PostResponse;
import com.lucas.example.feignhystrix.externalservice.response.UserResponse;
import com.lucas.example.feignhystrix.externalservice.service.JsonPlaceholderService;
import com.lucas.example.feignhystrix.helper.TestHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JsonPlaceholderServiceTest {

    private static final String ID = "1";
    @InjectMocks
    private JsonPlaceholderService service;

    @Mock
    private JsonPlaceholderClient gateway;

    @Test
    public void testGetUsers() {
        // given
        List<UserResponse> userResponses = TestHelper.createMockUserList(2);

        when(gateway.getUsers()).thenReturn(userResponses);

        // when
        List<UserResponse> response = service.getUsers();

        // then
        assertThat(response, is(equalTo(userResponses)));
        verify(gateway).getUsers();
    }

    @Test
    public void testGetPosts() {
        // given
        List<PostResponse> postResponses = TestHelper.createMockPostList(2);

        when(gateway.getPosts()).thenReturn(postResponses);

        // when
        List<PostResponse> response = service.getPosts();

        // then
        assertThat(response, is(equalTo(postResponses)));
        verify(gateway).getPosts();
    }

    @Test
    public void testGetUser() {
        // given
        UserResponse userResponse = TestHelper.createMockUserList(1).get(0);

        when(gateway.getUser(ID)).thenReturn(userResponse);

        // when
        UserResponse response = service.getUser(ID);

        // then
        assertThat(response, is(equalTo(userResponse)));
        verify(gateway).getUser(ID);
    }

    @Test
    public void testGetPost() {
        // given
        PostResponse postResponse = TestHelper.createMockPostList(1).get(0);

        when(gateway.getPost(ID)).thenReturn(postResponse);

        // when
        PostResponse response = service.getPost(ID);

        // then
        assertThat(response, is(equalTo(postResponse)));
        verify(gateway).getPost(ID);
    }
}
